window.onload = function () {
  var greeting = document.querySelector("#greetings h1");
  var smiley = document.querySelector("#smiley");
  var date = new Date();
  var hours = date.getHours();

  if (hours >= 5 && hours < 12) {
    greeting.textContent = "Good Morning!";
    smiley.innerHTML = "😊";
  } else if (hours >= 12 && hours < 18) {
    greeting.textContent = "Good Afternoon!";
    smiley.innerHTML = "😊";
  } else {
    greeting.textContent = "Good Evening!";
    smiley.innerHTML = "😊";
  }

  var subscribeButton = document.getElementById("subscribe-button");
  var subscribeIcon = document.getElementById("subscribe-icon");

  subscribeButton.addEventListener("click", function (event) {
    event.preventDefault();
    subscribeIcon.classList.remove("text-yellow-500");
    subscribeIcon.classList.add("text-green-500");
  });

  var themeSwitch = document.getElementById("theme-switch");

  themeSwitch.addEventListener("change", function (event) {
    if (event.target.checked) {
      document.body.classList.remove("light-theme");
      document.body.classList.add("dark-theme");
    } else {
      document.body.classList.remove("dark-theme");
      document.body.classList.add("light-theme");
    }
  });

  // Set the initial theme
  document.body.classList.add("light-theme");
};
